---
title: Digital Ethics
subtitle: How to Contribute to the Catalogue
---


You are welcome to contribute by

- Suggesting a new interesting resource about digital ethics
- Taking part in a discussion about one of the resources
- Voting the resources you find useful up and uninteresting resources down
- Developing the code behind Ethical Software Garden

In any case you will need an account at <https://gitlab.com/>.


# Suggest a New Resource

> TODO: What is a resource?
>
> TODO: What resources are intersting to us?

Got to the [list of issues][] and click "Add new issue" button. In the title provide a URL of the resource (if it has one) or title of a book, article, etc. Write a short description - what is it about, who authored it, etc.

The resource you added won't be published immediately. It is pending a review from one of the moderators.


# Share Your Thoughts

> TODO: What are our expectations regarding discussions?

Conversations around the resources are organized in discussions. You can start a new discussion or leave a reply in an existing one.

To start a new discussion go to the [list of issues][] and select the issue describing the resource you are interested in. At the bottom of the page there is a text area. Enter your comments there.

To take part in existing discussion find the opening remark and reply to it.


# Voting

If you find a resource useful, please let us know by voting it up. In the issue representing the resource click the thumbs up button. Resources with hight votes will be presented first on the page.

> TODO: What would be a good reason to vote a resource down?


# Help Us Develop the Page

Feel free to open a merge request if you think you can improve the code behind Ethical Software Garden.

> TODO: What about issues regarding the page? How do we separate them from resources?

[list of issues]: https://gitlab.com/software-garden/digital-ethics-resources/-/issues?sort=created_date&state=opened
