module Markdown exposing (render)

import Html exposing (Html)
import Markdown.Parser
import Markdown.Renderer


render : String -> Result String (List (Html msg))
render markdown =
    markdown
        |> Markdown.Parser.parse
        |> Result.mapError parseErrorsToString
        |> Result.andThen (Markdown.Renderer.render Markdown.Renderer.defaultHtmlRenderer)


parseErrorsToString errors =
    errors
        |> List.map Markdown.Parser.deadEndToString
        |> String.join "\n\n"
