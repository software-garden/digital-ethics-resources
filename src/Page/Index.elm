module Page.Index exposing
    ( Data
    , Model
    , Msg
    , page
    )

import Browser.Navigation
import DataSource exposing (DataSource)
import DataSource.File
import DataSource.Http
import Date exposing (Date)
import Dict exposing (Dict)
import Head
import Head.Seo as Seo
import Html exposing (Html)
import Html.Attributes
import Html.Custom as Html
import Http
import Iso8601
import Json.Encode
import List.Extra as List
import Markdown
import OptimizedDecoder
import Page exposing (Page, PageWithState, StaticPayload)
import Pages.PageUrl exposing (PageUrl)
import Pages.Secrets
import Pages.Url
import Path exposing (Path)
import Set exposing (Set)
import Shared
import Task
import Time
import Url
import View exposing (View)


type alias Model =
    { clock : Maybe Time.Posix
    , resources : List Resource
    , selectedLabels : Set String
    }


type Msg
    = ClockUpdated Time.Posix
    | GotResources (Result Http.Error (List Resource))
    | LabelSelectionChanged String Bool


type alias RouteParams =
    {}


page : PageWithState RouteParams Data Model Msg
page =
    Page.single
        { head = head
        , data = data
        }
        |> Page.buildWithLocalState
            { view = view
            , init = init
            , update = update
            , subscriptions = subscriptions
            }


init : Maybe PageUrl -> Shared.Model -> StaticPayload Data RouteParams -> ( Model, Cmd Msg )
init url_ model static =
    ( { clock = Nothing
      , resources = static.data.resources
      , selectedLabels = Set.empty
      }
    , Time.now |> Task.perform ClockUpdated
    )


update : PageUrl -> Maybe Browser.Navigation.Key -> Shared.Model -> StaticPayload Data RouteParams -> Msg -> Model -> ( Model, Cmd Msg )
update url_ maybeKey sharedModel static msg model =
    case msg of
        ClockUpdated time ->
            ( { model | clock = Just time }
            , getResources
            )

        GotResources (Err error) ->
            -- TODO: Handle error when getting resources
            ( model
            , Cmd.none
            )

        GotResources (Ok resources) ->
            ( { model | resources = resources }
            , Cmd.none
            )

        LabelSelectionChanged label selected ->
            if selected then
                ( { model | selectedLabels = Set.insert label model.selectedLabels }
                , Cmd.none
                )

            else
                ( { model | selectedLabels = Set.remove label model.selectedLabels }
                , Cmd.none
                )


subscriptions : Maybe PageUrl -> RouteParams -> Path -> Model -> Sub Msg
subscriptions maybeUrl route path model =
    Sub.none


data : DataSource Data
data =
    DataSource.map2 Data
        metadata
        resourcesData


metadata : DataSource Metadata
metadata =
    DataSource.File.bodyWithFrontmatter decodeMetadata "content/index.md"


resourcesData : DataSource (List Resource)
resourcesData =
    DataSource.Http.request
        (Pages.Secrets.succeed
            { url = "https://gitlab.com/api/graphql"
            , method = "POST"
            , headers = []
            , body = DataSource.Http.jsonBody resourcesRequestBody
            }
        )
        resourcesDecoder


getResources : Cmd Msg
getResources =
    Http.post
        { url = "https://gitlab.com/api/graphql"
        , body = Http.jsonBody resourcesRequestBody
        , expect = Http.expectJson GotResources <| OptimizedDecoder.decoder resourcesDecoder
        }


resourcesRequestBody : Json.Encode.Value
resourcesRequestBody =
    Json.Encode.object
        [ ( "query"
          , Json.Encode.string graphqlQuery
          )
        ]


graphqlQuery : String
graphqlQuery =
    """
{
  project(fullPath: "software-garden/digital-ethics-resources") {
    name
    description
    projectMembers {
      nodes {
        id
        user {
          id
          name
          avatarUrl
        }
        accessLevel {
          integerValue
          stringValue
        }
      }
    }
    issues(types: ISSUE, state: opened, assigneeId: "ANY") {
      nodes {
        id
        title
        description
        webUrl
        upvotes
        downvotes
        author {
          id
          name
          avatarUrl
        }
        labels {
          nodes {
            id
            color
            title
            description
          }
        }
        assignees {
          nodes {
            id
            name
            avatarUrl
          }
        }
        notes {
          nodes {
            id
            author {
              id
              name
              avatarUrl
            }
            createdAt
            body
            system
            discussion {
              id
            }
          }
        }
      }
    }
  }
}
"""


resourcesDecoder : OptimizedDecoder.Decoder (List Resource)
resourcesDecoder =
    OptimizedDecoder.at
        [ "data"
        , "project"
        , "issues"
        , "nodes"
        ]
        (OptimizedDecoder.list resourceDecoder)


resourceDecoder : OptimizedDecoder.Decoder Resource
resourceDecoder =
    OptimizedDecoder.succeed Resource
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "title" ] OptimizedDecoder.string)
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "assignees", "nodes" ] assigneesDecoder)
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "upvotes" ] OptimizedDecoder.int)
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "downvotes" ] OptimizedDecoder.int)
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "description" ] markdownDecoder)
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "notes", "nodes" ] decodeDiscussions)
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "labels", "nodes" ] labelsDecoder)


labelsDecoder : OptimizedDecoder.Decoder (List Label)
labelsDecoder =
    OptimizedDecoder.list labelDecoder


labelDecoder : OptimizedDecoder.Decoder Label
labelDecoder =
    OptimizedDecoder.succeed Label
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "title" ] OptimizedDecoder.string)
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "color" ] OptimizedDecoder.string)
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "description" ] (OptimizedDecoder.maybe OptimizedDecoder.string))


decodeDiscussions : OptimizedDecoder.Decoder (List ( Comment, List Comment ))
decodeDiscussions =
    OptimizedDecoder.list noteDecoder
        |> OptimizedDecoder.map getherDiscussions


getherDiscussions : List Note -> List ( Comment, List Comment )
getherDiscussions notes =
    notes
        |> List.filter (.system >> not)
        |> List.sortBy (.createdAt >> Time.posixToMillis)
        |> List.foldl insertNote Dict.empty
        |> Dict.values


insertNote :
    Note
    -> Dict String ( Comment, List Comment )
    -> Dict String ( Comment, List Comment )
insertNote note discussions =
    let
        comment : Comment
        comment =
            { author = note.author
            , content = note.body
            , timestamp = note.createdAt
            }
    in
    if note.system then
        discussions

    else
        case Dict.get note.discussion discussions of
            Nothing ->
                Dict.insert note.discussion
                    ( comment
                    , []
                    )
                    discussions

            Just ( start, replies ) ->
                Dict.insert note.discussion
                    ( start
                    , comment :: replies
                    )
                    discussions


type alias Note =
    { author : User
    , createdAt : Time.Posix
    , body : List (Html Msg)
    , system : Bool
    , discussion : String
    }


noteDecoder : OptimizedDecoder.Decoder Note
noteDecoder =
    OptimizedDecoder.succeed Note
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "author" ] authorDecoder)
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "createdAt" ] timeDecoder)
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "body" ] markdownDecoder)
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "system" ] OptimizedDecoder.bool)
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "discussion", "id" ] OptimizedDecoder.string)


timeDecoder : OptimizedDecoder.Decoder Time.Posix
timeDecoder =
    OptimizedDecoder.string
        |> OptimizedDecoder.map Iso8601.toTime
        |> OptimizedDecoder.map (Result.mapError <| always "Failed to parse the ISO8601 time string")
        |> OptimizedDecoder.andThen OptimizedDecoder.fromResult


markdownDecoder : OptimizedDecoder.Decoder (List (Html Msg))
markdownDecoder =
    OptimizedDecoder.string
        |> OptimizedDecoder.map Markdown.render
        |> OptimizedDecoder.andThen OptimizedDecoder.fromResult


assigneesDecoder : OptimizedDecoder.Decoder (List User)
assigneesDecoder =
    OptimizedDecoder.list assigneeDecoder


assigneeDecoder : OptimizedDecoder.Decoder User
assigneeDecoder =
    OptimizedDecoder.succeed User
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "name" ] OptimizedDecoder.string)
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "avatarUrl" ] OptimizedDecoder.string)


authorDecoder : OptimizedDecoder.Decoder User
authorDecoder =
    OptimizedDecoder.succeed User
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "name" ] OptimizedDecoder.string)
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.at [ "avatarUrl" ] OptimizedDecoder.string)


decodeMetadata : String -> OptimizedDecoder.Decoder Metadata
decodeMetadata markdown =
    markdown
        |> Markdown.render
        |> OptimizedDecoder.fromResult
        |> OptimizedDecoder.andThen metadataDecoder


metadataDecoder : List (Html Msg) -> OptimizedDecoder.Decoder Metadata
metadataDecoder body =
    OptimizedDecoder.succeed (Metadata body)
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.field
                "title"
                OptimizedDecoder.string
            )
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.field
                "subtitle"
                OptimizedDecoder.string
            )


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head static =
    Seo.summary
        { canonicalUrlOverride = Nothing
        , siteName = static.data.metadata.title
        , image =
            { url =
                "benjamin-suter-VRaxo0R1xMA-unsplash.jpg"
                    |> Path.fromString
                    |> Pages.Url.fromPath
            , alt = "Digital Ethics Resources"
            , dimensions = Just { width = 1920, height = 1280 }
            , mimeType = Just "image/jpeg"
            }
        , description = static.data.metadata.subtitle
        , locale = Just "en.US"
        , title = static.data.metadata.title
        }
        |> Seo.website


type alias Metadata =
    { introduction : List (Html Msg)
    , title : String
    , subtitle : String
    }


type alias Data =
    { metadata : Metadata
    , resources : List Resource
    }


type alias Resource =
    { name : String
    , moderators : List User
    , upvotes : Int
    , downvotes : Int
    , description : List (Html Msg)
    , discussions : List ( Comment, List Comment )
    , labels : List Label
    }


type alias Comment =
    { author : User
    , timestamp : Time.Posix
    , content : List (Html Msg)
    }


type alias User =
    { name : String
    , avatar : String
    }


type alias Label =
    { name : String
    , color : String
    , description : Maybe String
    }


view :
    Maybe PageUrl
    -> Shared.Model
    -> Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel model static =
    { title = static.data.metadata.title
    , body =
        [ viewHeader model.selectedLabels model.resources static.data.metadata
        , viewResources model.selectedLabels model.resources
        ]
    }


viewHeader : Set String -> List Resource -> Metadata -> Html Msg
viewHeader selectedLabels resources { title, subtitle, introduction } =
    [ title |> Html.text |> Html.wrap Html.h1 [ Html.Attributes.style "text-align" "center" ]
    , subtitle |> Html.text |> Html.wrap Html.h2 [ Html.Attributes.style "text-align" "center" ]
    , introduction |> Html.div []
    , viewLabelsSelection selectedLabels resources
    ]
        |> Html.header
            [ Html.Attributes.class "container"
            , Html.Attributes.style "padding-top" "8rem"
            , Html.Attributes.style "padding-bottom" "8rem"
            ]


viewLabelsSelection : Set String -> List Resource -> Html Msg
viewLabelsSelection selectedLabels resources =
    resources
        |> List.map .labels
        |> List.concat
        |> List.unique
        |> List.map (viewLabelSelector selectedLabels)
        |> (::) ("Pick the topics you are interested in. We will sort the resources accordingly. Or just scroll through all of them." |> Html.text |> Html.wrap Html.legend [])
        |> Html.fieldset
            [ Html.Attributes.style "display" "flex"
            , Html.Attributes.style "flex-wrap" "wrap"
            , Html.Attributes.style "gap" "1rem"
            ]


viewLabelSelector : Set String -> Label -> Html Msg
viewLabelSelector selected label =
    Html.checkbox
        label.name
        (LabelSelectionChanged label.name)
        (Set.member label.name selected)


viewResources : Set String -> List Resource -> Html Msg
viewResources selectedLabels resources =
    resources
        |> List.sortWith compareVotes
        |> List.sortWith (compareLabelsSelection selectedLabels)
        |> List.reverse
        |> List.map viewResource
        |> Html.main_ [ Html.Attributes.class "container" ]


compareLabelsSelection : Set String -> Resource -> Resource -> Order
compareLabelsSelection selected a b =
    compare
        (countLabels selected a.labels)
        (countLabels selected b.labels)


countLabels : Set String -> List Label -> Int
countLabels selected labels =
    labels
        |> List.map .name
        |> List.map (\name -> Set.member name selected)
        |> List.filter identity
        |> List.length


compareVotes : Resource -> Resource -> Order
compareVotes a b =
    compare
        (a.upvotes - a.downvotes)
        (b.upvotes - b.downvotes)


viewResource : Resource -> Html Msg
viewResource resource =
    let
        header =
            [ title
                |> Html.wrap Html.h3
                    [ Html.Attributes.style "margin-bottom" "1rem"
                    , Html.Attributes.style "font-size" "1.1rem"
                    ]
            , viewLabels resource
            ]
                |> Html.header
                    [ Html.Attributes.style "position" "sticky"
                    , Html.Attributes.style "top" "0"
                    , Html.Attributes.style "z-index" "1"
                    ]

        title : Html Msg
        title =
            case Url.fromString resource.name of
                Nothing ->
                    resource.name
                        |> Html.text

                Just url ->
                    resource.name
                        |> Html.text
                        |> Html.wrap Html.a
                            [ Html.Attributes.href resource.name
                            , Html.Attributes.target "_blank"
                            ]

        discussions =
            resource.discussions
    in
    [ header
    , viewDescription resource
    , viewDiscussions resource
    , viewMetadata resource
    ]
        |> Html.article [ Html.Attributes.class "resource" ]


viewLabels : Resource -> Html Msg
viewLabels resource =
    resource.labels
        |> List.map viewLabel
        |> Html.div [ Html.Attributes.style "display" "flex" ]


viewLabel : Label -> Html Msg
viewLabel label =
    let
        tooltip =
            case label.description of
                Nothing ->
                    []

                Just description ->
                    if
                        description
                            |> String.trim
                            |> String.isEmpty
                    then
                        []

                    else
                        description
                            |> Html.Attributes.attribute "data-tooltip"
                            |> List.singleton

        attributes =
            List.append
                tooltip
                [ Html.Attributes.style "padding" "0.2em 1em"
                , Html.Attributes.style "border-radius" "1em"
                , Html.Attributes.style "border-color" label.color
                , Html.Attributes.style "border-width" "3px"
                , Html.Attributes.style "border-style" "solid"
                , Html.Attributes.style "margin" "0.4em"
                , Html.Attributes.style "font-size" "0.6em"
                , Html.Attributes.style "font-weight" "bold"
                ]
    in
    label.name
        |> Html.text
        |> Html.wrap Html.div
            attributes


viewDiscussions : Resource -> Html Msg
viewDiscussions resource =
    resource.discussions
        |> List.map viewDiscussion
        |> Html.div []


viewDiscussion : ( Comment, List Comment ) -> Html Msg
viewDiscussion ( start, replies ) =
    [ start |> viewComment
    , viewReplies replies
    ]
        |> Html.section []


viewReplies : List Comment -> Html Msg
viewReplies replies =
    let
        summary text =
            text
                |> Html.text
                |> Html.wrap Html.summary [ Html.Attributes.style "text-align" "end" ]
    in
    case replies of
        [] ->
            Html.hr [] []

        [ single ] ->
            replies
                |> List.sortBy (.timestamp >> Time.posixToMillis)
                |> List.map viewComment
                |> (::) (summary "One response")
                |> Html.details []

        _ ->
            replies
                |> List.sortBy (.timestamp >> Time.posixToMillis)
                |> List.map viewComment
                |> (::)
                    ([ replies |> List.length |> String.fromInt
                     , " responses"
                     ]
                        |> String.join ""
                        |> summary
                    )
                |> Html.details []


viewComment : Comment -> Html Msg
viewComment comment =
    [ viewCommentAvatar comment.author
    , [ viewCommentMetadata comment
      , viewCommentBody comment
      ]
        |> Html.div [ Html.Attributes.style "width" "100%" ]
    ]
        |> Html.div [ Html.Attributes.style "display" "flex" ]


viewCommentBody : Comment -> Html Msg
viewCommentBody comment =
    comment.content |> Html.div [ Html.Attributes.class "comment" ]


viewCommentMetadata : Comment -> Html msg
viewCommentMetadata comment =
    [ comment.author.name |> Html.text |> Html.wrap Html.strong []
    , [ " at " |> Html.text
      , comment.timestamp |> Iso8601.fromTime |> Html.text
      ]
        |> Html.small []
    ]
        |> Html.h5
            [ Html.Attributes.style "display" "flex"
            , Html.Attributes.style "flex-wrap" "wrap"
            , Html.Attributes.style "justify-content" "space-between"
            , Html.Attributes.style "align-items" "baseline"
            , Html.Attributes.style "font-size" "1rem"
            ]


viewCommentAvatar : User -> Html msg
viewCommentAvatar author =
    let
        -- Sometimes avatar URL is absolute, but sometimes it's relative to https://gitlab.com/
        avatarUrl : String
        avatarUrl =
            author.avatar
                |> Url.fromString
                |> Maybe.map Url.toString
                |> Maybe.withDefault ("https://gitlab.com/" ++ author.avatar)
    in
    Html.img
        [ Html.Attributes.src avatarUrl
        , Html.Attributes.alt author.name
        , Html.Attributes.style "height" "2em"
        , Html.Attributes.style "border-radius" "100%"
        , Html.Attributes.style "margin" "0em 1em"
        , Html.Attributes.style "vertical-align" "top"
        ]
        []


viewMetadata : Resource -> Html Msg
viewMetadata resource =
    [ viewVotes resource
    , viewModerators resource.moderators
    ]
        |> Html.footer
            [ Html.Attributes.style "display" "flex"
            , Html.Attributes.style "justify-content" "space-between"
            ]


viewDescription : Resource -> Html Msg
viewDescription resource =
    resource.description
        |> Html.section []


viewModerators : List User -> Html Msg
viewModerators moderators =
    [ "Moderators: "
        |> Html.text
        |> Html.wrap Html.strong [ Html.Attributes.style "text-align" "end" ]
    , moderators
        |> List.map viewModerator
        |> Html.div
            [ Html.Attributes.style "display" "flex"
            , Html.Attributes.style "flex-direction" "column"
            ]
    ]
        |> Html.div
            [ Html.Attributes.style "display" "flex"
            , Html.Attributes.style "flex-wrap" "wrap"
            , Html.Attributes.style "justify-content" "end"
            , Html.Attributes.style "gap" "1rem"
            ]


viewModerator : User -> Html Msg
viewModerator moderator =
    let
        -- Sometimes avatar URL is absolute, but sometimes it's relative to https://gitlab.com/
        avatarUrl : String
        avatarUrl =
            moderator.avatar
                |> Url.fromString
                |> Maybe.map Url.toString
                |> Maybe.withDefault ("https://gitlab.com/" ++ moderator.avatar)
    in
    [ Html.img
        [ Html.Attributes.src avatarUrl
        , Html.Attributes.alt moderator.name
        , Html.Attributes.style "height" "1em"
        , Html.Attributes.style "border-radius" "100%"
        , Html.Attributes.style "margin" "0em 1em"
        , Html.Attributes.style "vertical-align" "middle"
        ]
        []
    , moderator.name |> Html.text
    ]
        |> Html.span []


viewVotes : Resource -> Html Msg
viewVotes resource =
    [ [ "👍 ⨉ "
      , resource.upvotes |> String.fromInt
      ]
        |> String.join ""
        |> Html.text
        |> Html.wrap Html.div []
    , [ "👎 ⨉ "
      , resource.downvotes |> String.fromInt
      ]
        |> String.join ""
        |> Html.text
        |> Html.wrap Html.div []
    ]
        |> Html.div
            [ Html.Attributes.style "display" "flex"
            , Html.Attributes.style "flex-wrap" "wrap"
            , Html.Attributes.style "align-content" "start"
            , Html.Attributes.style "gap" "1rem"
            ]
