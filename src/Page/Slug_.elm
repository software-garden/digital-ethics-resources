module Page.Slug_ exposing (Data, Model, Msg, contentData, page)

import DataSource exposing (DataSource)
import DataSource.File
import DataSource.Glob
import Dict exposing (Dict)
import Head
import Head.Seo as Seo
import Html exposing (Html)
import Html.Attributes
import Html.Custom as Html
import Markdown
import OptimizedDecoder
import Page exposing (Page, PageWithState, StaticPayload)
import Pages.PageUrl exposing (PageUrl)
import Pages.Url
import Path
import Shared
import View exposing (View)


type alias Model =
    ()


type alias Msg =
    Never


type alias RouteParams =
    { slug : String }


page : Page RouteParams Data
page =
    Page.prerender
        { head = head
        , routes = routes
        , data = data
        }
        |> Page.buildNoState { view = view }


contentData : DataSource (Dict String String)
contentData =
    DataSource.Glob.succeed Tuple.pair
        |> DataSource.Glob.match (DataSource.Glob.literal "content/pages/")
        |> DataSource.Glob.capture DataSource.Glob.wildcard
        |> DataSource.Glob.match (DataSource.Glob.literal ".md")
        |> DataSource.Glob.captureFilePath
        |> DataSource.Glob.toDataSource
        |> DataSource.map Dict.fromList


routes : DataSource (List RouteParams)
routes =
    contentData
        |> DataSource.map Dict.keys
        |> DataSource.map (List.map RouteParams)


data : RouteParams -> DataSource Data
data routeParams =
    contentData
        |> DataSource.map (Dict.get routeParams.slug)
        |> DataSource.map (Result.fromMaybe "No path found for given slug")
        |> DataSource.andThen DataSource.fromResult
        |> DataSource.andThen (DataSource.File.bodyWithFrontmatter contentDecoder)


contentDecoder : String -> OptimizedDecoder.Decoder Document
contentDecoder markdown =
    markdown
        |> Markdown.render
        |> OptimizedDecoder.fromResult
        |> OptimizedDecoder.andThen dataDecoder


dataDecoder : List (Html Msg) -> OptimizedDecoder.Decoder Document
dataDecoder body =
    OptimizedDecoder.succeed Document
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.field
                "title"
                OptimizedDecoder.string
            )
        |> OptimizedDecoder.andMap
            (OptimizedDecoder.field
                "subtitle"
                OptimizedDecoder.string
            )
        |> OptimizedDecoder.map
            (\document -> document body)


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head static =
    Seo.summary
        { canonicalUrlOverride = Nothing
        , siteName = static.data.title
        , image =
            { url =
                "benjamin-suter-VRaxo0R1xMA-unsplash.jpg"
                    |> Path.fromString
                    |> Pages.Url.fromPath
            , alt = "Digital Ethics Resources"
            , dimensions = Just { width = 1920, height = 1280 }
            , mimeType = Just "image/jpeg"
            }
        , description = static.data.subtitle
        , locale = Just "en.US"
        , title = static.data.title
        }
        |> Seo.website


type alias Data =
    Document


type alias Document =
    { title : String
    , subtitle : String
    , body : List (Html Msg)
    }


view :
    Maybe PageUrl
    -> Shared.Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel static =
    { title = static.data.title
    , body =
        [ viewHeader static.data
        , viewMain static.data.body
        ]
    }


viewHeader : Document -> Html Msg
viewHeader { title, subtitle } =
    [ title
        |> Html.text
        |> Html.wrap Html.a [ Html.Attributes.href "/" ]
        |> Html.wrap Html.h1 [ Html.Attributes.style "text-align" "center" ]
    , subtitle |> Html.text |> Html.wrap Html.h2 [ Html.Attributes.style "text-align" "center" ]
    ]
        |> Html.header
            [ Html.Attributes.class "container"
            , Html.Attributes.style "padding-top" "8rem"
            , Html.Attributes.style "padding-bottom" "8rem"
            ]


viewMain : List (Html Msg) -> Html Msg
viewMain body =
    body
        |> Html.main_ [ Html.Attributes.class "container" ]
